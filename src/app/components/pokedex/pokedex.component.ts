import { Component } from '@angular/core';
import { Pokemon } from '../../models/pokemon';
import { bulbasaur } from '../../../assets/bulbasaur';
import { ControlsComponent } from './left-screen/controls/controls.component';
import { ScreenComponent } from './left-screen/screen/screen.component';
import { DescriptionComponent } from './right-screen/description/description.component';
import { NumpadComponent } from './right-screen/numpad/numpad.component';
import { StatsComponent } from './right-screen/stats/stats.component';
import { LightsComponent } from './left-screen/lights/lights.component';

@Component({
  selector: 'app-pokedex',
  standalone: true,
  imports: [
    LightsComponent,
    ScreenComponent,
    ControlsComponent,
    DescriptionComponent,
    NumpadComponent,
    StatsComponent,
  ],
  templateUrl: './pokedex.component.html',
  styleUrl: './pokedex.component.scss',
})
export class PokedexComponent {
  public pokemon: Pokemon = bulbasaur;
}
