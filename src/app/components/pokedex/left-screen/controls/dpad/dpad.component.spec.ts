import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DpadComponent } from './dpad.component';

describe('DpadComponent', () => {
  let component: DpadComponent;
  let fixture: ComponentFixture<DpadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DpadComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DpadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
