import { Component } from '@angular/core';
import { DpadComponent } from './dpad/dpad.component';

@Component({
  selector: 'app-controls',
  standalone: true,
  imports: [DpadComponent],
  templateUrl: './controls.component.html',
  styleUrl: './controls.component.scss'
})
export class ControlsComponent {

}
