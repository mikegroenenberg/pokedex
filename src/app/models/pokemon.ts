export interface Pokemon {
  id: number;
  name: string;
  height: number;
  weight: number;
  description: string;
  sprites: Sprites;
  types: Type[];
}

export interface Sprites {
  frontDefault: string;
  backDefault: string;
  frontShiny: string;
  backShiny: string;

  frontFemale?: string | null;
  backFemale?: string | null;
  frontShinyFemale?: string | null;
  backShinyFemale?: string | null;
}

export interface Type {
  name: string;
  url: string;
}
