import { Pokemon } from '../app/models/pokemon';

export const bulbasaur: Pokemon = {
  id: 1,
  name: 'bulbasaur',
  description: 'Voegen we later toe',
  height: 7,
  weight: 69,
  sprites: {
    backDefault:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png',
    backFemale: null,
    backShiny:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/1.png',
    backShinyFemale: null,
    frontDefault:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
    frontFemale: null,
    frontShiny:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png',
    frontShinyFemale: null,
  },
  types: [
    {
      name: 'grass',
      url: 'https://pokeapi.co/api/v2/type/12/',
    },
    {
      name: 'poison',
      url: 'https://pokeapi.co/api/v2/type/4/',
    },
  ],
};
